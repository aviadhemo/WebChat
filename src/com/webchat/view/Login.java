package com.webchat.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldUserName;
	private JTextField textFieldAddress;
	private JLabel lblIpAddress;
	private JTextField textFieldPort;
	private JLabel lblPort;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		setResizable(false);
		setTitle("Chat Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 380);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textFieldUserName = new JTextField();
		textFieldUserName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldUserName.setToolTipText("Name");
		textFieldUserName.setBounds(49, 43, 179, 25);
		contentPane.add(textFieldUserName);
		textFieldUserName.setColumns(10);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(49, 23, 56, 16);
		contentPane.add(lblName);
		
		textFieldAddress = new JTextField();
		textFieldAddress.setToolTipText("IP Address");
		textFieldAddress.setBounds(49, 103, 179, 25);
		contentPane.add(textFieldAddress);
		textFieldAddress.setColumns(10);
		
		lblIpAddress = new JLabel("IP Address:");
		lblIpAddress.setBounds(49, 81, 91, 16);
		contentPane.add(lblIpAddress);
		
		textFieldPort = new JTextField();
		textFieldPort.setToolTipText("Port Number");
		textFieldPort.setColumns(10);
		textFieldPort.setBounds(49, 163, 179, 25);
		contentPane.add(textFieldPort);
		
		lblPort = new JLabel("Port:");
		lblPort.setBounds(49, 141, 91, 16);
		contentPane.add(lblPort);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = textFieldUserName.getText();
				String address = textFieldAddress.getText();
				int port = 0;
				
				try {
					Integer.parseInt(textFieldPort.getText());
				} catch(NumberFormatException ex) {
					ex.printStackTrace();
				}
				
				login(name, address, port);
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnLogin.setBounds(49, 234, 179, 38);
		contentPane.add(btnLogin);
	}
	
	private void login(String name, String address, int port) {
		this.dispose();
		new Client(name, address, port);
	}
}
